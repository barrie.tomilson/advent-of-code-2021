# load data.txt
with open("C:/Users/Barrie Tomilson/Documents/github/AOC/advent-of-code-2021/Day1/data.txt") as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

print(lines)
count = 0
# loop starting on index 1
for i, line in enumerate(lines):
    if i < len(lines)-1:
        next_line = int(lines[i+1])
        if int(line) < next_line:
            count += 1
print(count)
