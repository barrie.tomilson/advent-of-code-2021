# load data.txt
with open("C:/Users/Barrie Tomilson/Documents/github/AOC/advent-of-code-2021/Day2/part1/data.txt") as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

print(lines)
x_position = 0
y_position = 0
aim = 0


# loop starting on index 1
for i, line in enumerate(lines):
    print(line)
    direction, value = line.split(" ")
    value = int(value)
    if direction.lower() == "down":
        y_position += value
    if direction.lower() == "up":
        y_position -= value
    if direction.lower() == "forward":
        x_position += value

    # split command into direction and value
    # increment x or y by value

# then multiply x by y
print(x_position)
print(y_position)
print(x_position * y_position)
