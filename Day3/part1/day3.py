# load data.txt
with open("C:/Users/Barrie Tomilson/Documents/github/AOC/advent-of-code-2021/Day3/part1/data.txt") as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]


position = [[],[],[],[],[],[],[],[],[],[],[],[]]

# I want to reorient the digits in lines so that at position 0, there's a list containing all values at that position

for line in lines:
    for i in range(0, len(line)):
        position[i].append(int(line[i]))
        


def most_common(lst):
    return max(set(lst), key=lst.count)
def least_common(lst):
    return min(set(lst), key=lst.count)

gamma = ""
epsilon = ""
for arr in position:
    gamma_digit = str(most_common(arr))
    epsilon_digit = str(least_common(arr))
    gamma+=gamma_digit
    epsilon+=epsilon_digit

print(int(gamma, 2) * int(epsilon, 2))
