# load data.txt
with open("C:/Users/Barrie Tomilson/Documents/github/AOC/advent-of-code-2021/Day3/part2/data.txt") as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]



def generate_positions(lines):
    position = [[],[],[],[],[], [],[],[],[],[], [], []]
    
    for line in lines:
        for i in range(0, len(line)):
            position[i].append(int(line[i]))
    return position    


def most_common(lst):

    counts = np.bincount(lst)
    if np.argmax(counts) == np.argmin(counts):
        return 1
    return np.argmax(counts)

def least_common(lst):
    counts = np.bincount(lst)
    if np.argmax(counts) == np.argmin(counts):
        return 0
    return np.argmin(counts)



import numpy as np
oxygen_positions = np.array(generate_positions(lines))
newlist = []
position_tracker = 0
while position_tracker < len(oxygen_positions):
    ox_dig = int(most_common(oxygen_positions[position_tracker]))
    indeces = np.where(oxygen_positions[position_tracker] != ox_dig)
    oxygen_positions = np.delete(oxygen_positions, indeces, axis=1)
    position_tracker+=1

scrubber_positions = np.array(generate_positions(lines))
newlist = []
position_tracker = 0
while position_tracker < len(scrubber_positions):
    if scrubber_positions.shape[1] == 1:
        break
    dig = int(least_common(scrubber_positions[position_tracker]))
    indeces = np.where(scrubber_positions[position_tracker] != dig)
    scrubber_positions = np.delete(scrubber_positions, indeces, axis=1)

    position_tracker+=1



ox = int(''.join(map(str,list(oxygen_positions.ravel()))), 2)
scrub = int(''.join(map(str,list(scrubber_positions.ravel()))), 2)


print(ox * scrub)
