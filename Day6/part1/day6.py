def pop_handler(counter):
    old_counter = dict(counter)
    # all the 0s become 8s and get added to 6s
    val = int(counter[0])

    # all the 1's become 0s
    counter[0] = counter[1] 
    # all the 2's become 1s
    counter[1] = counter[2] 
    # all the 3's become 2's
    counter[2] = counter[3] 

    # all the 4's become 3's
    counter[3] = counter[4] 

    # all the 5's become 4's
    counter[4] = counter[5] 

    # all the 6's become 5's
    counter[5] = counter[6] 

    # all the 7's become 6's
    counter[6] = counter[7] +val

    # all the 8's become 7's
    counter[7] = counter[8] 
    counter[8] = val

    return counter


data = [3,5,1,5,3,2,1,3,4,2,5,1,3,3,2,5,1,3,1,5,5,1,1,1,2,4,1,4,5,2,1,2,4,3,1,2,3,4,3,4,4,5,1,1,1,1,5,5,3,4,4,4,5,3,4,1,4,3,3,2,1,1,3,3,3,2,1,3,5,2,3,4,2,5,4,5,4,4,2,2,3,3,3,3,5,4,2,3,1,2,1,1,2,2,5,1,1,4,1,5,3,2,1,4,1,5,1,4,5,2,1,1,1,4,5,4,2,4,5,4,2,4,4,1,1,2,2,1,1,2,3,3,2,5,2,1,1,2,1,1,1,3,2,3,1,5,4,5,3,3,2,1,1,1,3,5,1,1,4,4,5,4,3,3,3,3,2,4,5,2,1,1,1,4,2,4,2,2,5,5,5,4,1,1,5,1,5,2,1,3,3,2,5,2,1,2,4,3,3,1,5,4,1,1,1,4,2,5,5,4,4,3,4,3,1,5,5,2,5,4,2,3,4,1,1,4,4,3,4,1,3,4,1,1,4,3,2,2,5,3,1,4,4,4,1,3,4,3,1,5,3,3,5,5,4,4,1,2,4,2,2,3,1,1,4,5,3,1,1,1,1,3,5,4,1,1,2,1,1,2,1,2,3,1,1,3,2,2,5,5,1,5,5,1,4,4,3,5,4,4]

days = 256


from collections import Counter
c = Counter(data)

for day in range(0, days):
    c = pop_handler(c)


tot = 0
for i in c:
    tot += c[i]

print(tot)

