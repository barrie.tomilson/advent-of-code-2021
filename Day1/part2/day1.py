# load data.txt
with open("C:/Users/Barrie Tomilson/Documents/github/AOC/advent-of-code-2021/Day1/part2/data.txt") as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

lines = list(map(int, lines))

print(lines)
count = 0

# loop starting on index 1
for i in range(0, len(lines)-3):
    line_1_tot = sum(lines[i:i+3])
    line_2_tot = sum(lines[i+1:i+4])
    if line_1_tot < line_2_tot:
        count+=1
        # save total


print(count)
